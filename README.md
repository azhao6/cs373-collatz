Name: Alexander Zhao

EID: awz222

GitLab ID: azhao6

HackerRank ID: syngerical

Git SHA: 0afdc626b6dda14ab3d60d60516ac50074f1c3e5 

GitLab Piplines: https://gitlab.com/azhao6/cs373-collatz/pipelines 

Estimated Completion Time: 4 hours

Actual Completion Time: 3 hours

Comments: This project was fairly straight forward since I took OOP last semester but was still good practice.
